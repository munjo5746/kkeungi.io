FROM python:3.7-alpine

WORKDIR /src


RUN apk add --update nodejs npm
RUN apk add yarn
RUN wget https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-281.0.0-linux-x86_64.tar.gz
RUN tar -xzf google-cloud-sdk-281.0.0-linux-x86_64.tar.gz
RUN ./google-cloud-sdk/bin/gcloud auth activate-service-account --key-file "$SERVICE_ACCOUNT_KEY_FILE"
RUN ./google-cloud-sdk/bin/gsutil -m cp ./build/* gs://www.kkeungi.com # here, -m option to gsutil make operations run in parallel


