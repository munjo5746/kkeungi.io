# koder.io

## What is it?
Koder.io will be a single page where it shows your coding activity based on the GitHub event. Depending on the type of information we can get from GitHub API, there will be different type of information displayed on the page.

## Why?
When I have to go on a job hunting, it has been pain in the ass proving myself about coding knowledge to a non-technical recruiter.  Also, it is stressful when non-technical recruiter asks questions that are not related to the job position they are trying to fill out. This project started in the hope that for coders who are not great speaker but have passion on coding to prove their knowledge to those people who need the skillsets. 

## How?
This is personally opinionated but I think in order to be a great coder, the person has to have a coding habit. The habit they do inside/outside of their work whenever they are free. In order to prove this to someone who does not know about the coder, it is good start to see the coding activities. There might be other type of coding activity info available, but decided to start with the GitHub commits. The GitHub statistics API provides the following information.

1. Get contributors list with additions, deletions, and commit counts
2. Get the last year of commit activity data
3. Get the number of additions and deletions per week
4. Get the weekly commit count for the repository owner and everyone else
5. Get the number of commits per hour in each day

I decided to start with the option #5 because it gives the day, time and number of commits. [[Statistics | GitHub Developer Guide](https://developer.github.com/v3/repos/statistics/#get-contributors-list-with-additions-deletions-and-commit-counts)]

## Techs
- [ ] Nivo for data visualization
- [ ] Typescript
- [ ] Gitlab for CI/CD
- [ ] Github API
- [ ] Ant Design [Ant Design - The world&#x27;s second most popular React UI framework](https://ant.design/)

## Ideas
- [ ] Summary of the coder’s experiences
