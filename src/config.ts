import { IConfig, IGetMetricParameterType } from './shared/types/config';

// GCP info
// Those values are coming from .env which will be secret.
const REGION = process.env.REACT_APP_REGION;
const PROJECT_ID = process.env.REACT_APP_PROEJCT_ID;

// Define urls
// The hostUrl will be the final host url that will be used to send requests.
// Also, assumes that it only has local and production environments.
const githubHost = 'https://api.github.com';
const apiHostDev = 'https://nodeapp-qr34nszoxa-uc.a.run.app';
const apiHostLocal = 'http://localhost:5000';

const hostUrl =
  (process.env.NODE_ENV === 'test' && apiHostLocal) ||
  (process.env.NODE_ENV === 'development' && apiHostDev) ||
  (process.env.NODE_ENV === 'production' && apiHostDev) ||
  null;

if (!hostUrl)
  throw new Error('hostUrl must be defined. Check your environment.');

const config: IConfig = {
  services: {
    authorization: (loginHostUrl: string = githubHost, clientId: string) =>
      `${loginHostUrl}/authorizations/clients/${clientId}`,
    auth: {
      getSocialLoginTokenUrl: ({ host }) => `${host}/auth/token`,
      getUser: ({ id }) => `${hostUrl}/auth/users/${id}`
    },
    metrics: {
      getMetricUrl: ({
        host = hostUrl,
        metricType,
        userID,
        queryParameters
      }: IGetMetricParameterType) =>
        `${host}/metrics/${metricType}/users/${userID}?${queryParameters || ''}`
    }
  }
};

export default config;
