import { ThunkAction } from 'redux-thunk';
import {
  AuthActionTypes,
  ErrorCode,
  IUser,
  LoginTypes
} from '../../shared/types/auth';
import { Action, Dispatch } from 'redux';

import services from '../../services/index';
import { delay } from '../../shared/utils';
import config from '../../config';

import { MetricTypeInfo, MetricType } from '../../shared/types/metrics';

const toUser = (userInResponse: any): IUser => {
  const {
    id,
    email,
    first_name: firstName,
    last_name: lastName,
    about
  } = userInResponse;

  return {
    id,
    email,
    firstName,
    lastName,
    roles: [],
    about
  };
};

export const login = (
  loginType: LoginTypes,
  parameters?: any
): ThunkAction<void, any, null, Action<string>> => async dispatch => {
  if (!parameters) {
    throw new Error(
      'The parameters passed in have to be validated prior to the function call.'
    );
  }

  const { code } = parameters;

  dispatch(loginBegin());

  await delay(1000);

  switch (loginType) {
    case 'github':
      const { result, error } = await services.auth.githubLogin(code);

      if (error) {
        return dispatch(loginFail(error));
      }

      const { user, accessToken } = result;

      localStorage.setItem('kkeungi', accessToken);
      localStorage.setItem('kkeungi_user_id', user.id);

      dispatch(
        loginSuccess({
          id: user.id,
          email: user.email,
          firstName: user.firstName || '',
          lastName: user.lastName || '',
          roles: user.roles || [],
          about: user.about || ''
        })
      );

      break;
    default:
      throw new Error('Unknow login type.');
  }
};

const loginBegin = () => {
  return {
    type: AuthActionTypes.LOGIN_BEGIN,
    payload: null
  };
};

const loginFail = (error: { message: string }) => {
  return {
    type: AuthActionTypes.LOGIN_FAIL,
    payload: {
      error
    }
  };
};

const loginSuccess = (user: IUser) => {
  return {
    type: AuthActionTypes.LOGIN_SUCCESS,
    payload: {
      user
    }
  };
};

export const logout = (): ThunkAction<
  void,
  any,
  null,
  Action<string>
> => async dispatch => {
  dispatch(logoutBegin());

  await delay(1000);

  localStorage.removeItem('kkeungi_user_id');

  dispatch(logoutComplete());
};

const logoutBegin = () => {
  return {
    type: AuthActionTypes.LOGOUT_BEGIN,
    payload: null
  };
};

const logoutComplete = () => {
  return {
    type: AuthActionTypes.LOGOUT_COMPLETE,
    payload: null
  };
};

export const init = () => async (dispatch: any) => {
  dispatch(initBegin());

  await delay(1000);

  const userID = 1;

  // TODO: below user instance has to be retrieved from db
  const user: IUser = {
    id: 1,
    email: 'munjo5746@gmail.com',
    firstName: 'Edward',
    lastName: 'Chung',
    roles: ['Software Engineer', 'Full stack developer'],
    about: '-'
  };

  dispatch(
    initSuccess({
      id: user.id,
      email: user.email,
      firstName: user.firstName || '-',
      lastName: user.lastName || '-',
      roles: user.roles || [],
      about: user.about || '-'
    })
  );

  const { metrics: metricServices } = config.services;

  const typeOfMetrics = MetricTypeInfo.map(metricTitle => metricTitle.type);

  dispatch(getMetricsBegin(typeOfMetrics));

  const metrics = typeOfMetrics.map(metricType => ({
    metric: metricType as any,
    url: metricServices.getMetricUrl({
      metricType,
      userID,
      queryParameters: `type=github&metric=${metricType}`
    })
  }));

  services.metrics.getMetrics(metrics, metricData => {
    // fill
  });
};

const initBegin = () => {
  return {
    type: AuthActionTypes.INIT_BEGIN,
    payload: null
  };
};

const initSuccess = (user: IUser) => {
  return {
    type: AuthActionTypes.INIT_SUCCESS,
    payload: {
      user
    }
  };
};

const getMetricsBegin = (metricTypes: MetricType[]) => {
  return {
    type: AuthActionTypes.GET_METRICS_BEGIN,
    payload: {
      metricTypes
    }
  };
};

const initMetricsComplete = (
  metricData: Array<{
    metric: MetricType;
    data: any;
  }>
) => {
  return {
    type: AuthActionTypes.INIT_METRICS_COMPLETE,
    payload: {
      metricData
    }
  };
};

export const saveUser = (user: IUser) => async (dispatch: any) => {
  dispatch(saveUserBegin());

  await delay(500);

  const response = await services.auth.saveUser(user);

  const { result, error } = response;

  if (error) {
    return dispatch(saveUserFail(error));
  }

  const { user: userInResponse } = result;

  return dispatch(saveUserSuccess(toUser(userInResponse)));
};

const saveUserBegin = () => {
  return {
    type: AuthActionTypes.SAVE_USER_BEGIN,
    payload: null
  };
};

const saveUserFail = (error: { message: string; code: ErrorCode }) => {
  return {
    type: AuthActionTypes.SAVE_USER_FAIL,
    payload: {
      error
    }
  };
};

const saveUserSuccess = (user: IUser) => {
  return {
    type: AuthActionTypes.SAVE_USER_SUCCESS,
    payload: {
      user
    }
  };
};
