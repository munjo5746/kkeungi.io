import * as authActions from './auth';

export default {
  auth: {
    ...authActions
  }
};
