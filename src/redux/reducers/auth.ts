import { AuthActionTypes } from '../../shared/types/auth';
import { MetricType } from '../../shared/types/metrics';

export interface IAuthState {
  user?: any;
  loading: boolean;
  logoutInProgress: boolean;
  redirectPath?: string;
  savingUser: boolean;
  metricData: Array<{
    metric: 'week-commits';
    data: any;
  }>;
  fetchingMetrics: MetricType[];
  error?: { message: string; data: any };
}

const initialState: IAuthState = {
  loading: false,
  logoutInProgress: false,
  savingUser: false,
  metricData: [],
  fetchingMetrics: [],
  error: undefined
};

export default (state = initialState, action: any): IAuthState => {
  const { type, payload } = action;
  const { user, error } = payload || { user: null, error: null };

  switch (type) {
    case AuthActionTypes.LOGIN_BEGIN:
      return {
        ...state,
        loading: true
      };

    case AuthActionTypes.LOGIN_SUCCESS:
      return {
        ...state,
        loading: false,
        redirectPath: '/',
        user
      };

    case AuthActionTypes.LOGIN_FAIL:
      return {
        ...state,
        loading: false,
        error
      };

    case AuthActionTypes.LOGOUT_BEGIN:
      return {
        ...state,
        logoutInProgress: true
      };

    case AuthActionTypes.LOGOUT_COMPLETE:
      return {
        ...state,
        logoutInProgress: false,
        user: undefined
      };

    case AuthActionTypes.INIT_BEGIN:
      return {
        ...state,
        loading: true,
        redirectPath: undefined,
        fetchingMetrics: []
      };

    case AuthActionTypes.INIT_EXAMPLE:
      return {
        ...state,
        loading: false
      };

    case AuthActionTypes.INIT_FAIL:
      return {
        ...state,
        loading: false,
        error,
        redirectPath: payload.redirectPath
      };

    case AuthActionTypes.INIT_SUCCESS:
      return {
        ...state,
        user,
        loading: false,
        redirectPath: undefined
      };
    case AuthActionTypes.INIT_METRICS_COMPLETE:
      const { metricData } = payload;

      return {
        ...state,
        metricData
      };

    case AuthActionTypes.GET_METRICS_BEGIN:
      return {
        ...state,
        fetchingMetrics: payload.metricTypes as MetricType[]
      };

      break;
    case AuthActionTypes.SAVE_USER_BEGIN:
      return {
        ...state,
        savingUser: true
      };

    case AuthActionTypes.SAVE_USER_FAIL:
      return {
        ...state,
        savingUser: false,
        error
      };

    case AuthActionTypes.SAVE_USER_SUCCESS:
      return {
        ...state,
        savingUser: false,
        user
      };
    case AuthActionTypes.REDIRECT:
      const { redirectPath } = payload;

      return {
        ...state,
        loading: false,
        redirectPath
      };
    default:
      return state;
  }
};
