import React from 'react';
import { Result } from 'antd';

const _500: React.FC = () => {
  return <Result status="500" title="Opps! There was server error!" />;
};

export default _500;
