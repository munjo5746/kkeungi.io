import { Col, notification, Row } from 'antd';
import React, { useEffect, useCallback } from 'react';
import { connect } from 'react-redux';

import Profile from './app/Profile';

import { Redirect } from 'react-router';
import PageLoading from './app/PageLoading';

import actions from '../redux/actions';

import { LoginTypes } from '../shared/types/auth';
import { IAuthState } from '../redux/reducers/auth';
import { MetricType, MetricTypes } from '../shared/types/metrics';
import Metric from './app/Metric';

interface IApp {
  metricData: Array<{
    metric: 'week-commits';
    data: any;
  }>;
  location: any;
  loading: boolean;
  redirectPath?: string;
  fetchingMetrics: MetricType[];

  error?: { message: string };

  // functions
  login: (loginType: LoginTypes, paramters?: any) => void;
  init: () => void;
}

const App: React.FC<IApp> = ({
  metricData,
  location,
  loading,
  redirectPath,
  login,
  fetchingMetrics,
  error,
  init
}) => {
  useEffect(() => {
    init();
    // eslint-disable-next-line
  }, []);

  const { search } = location;
  const redirectToGithubLoginPage = useCallback(() => {
    if (!search) return;

    // if code=<code> is in the queryString, it is github login.
    const loginType: LoginTypes =
      search.indexOf('code=') !== -1 ? 'github' : 'gitlab';
    const code = search.replace(/\?code=/g, '');

    login(loginType, { code });
  }, [search, login]);

  useEffect(redirectToGithubLoginPage, [search]);

  const displayErrorMessage = useCallback(() => {
    if (!error) return;

    const { message } = error;

    notification.error({
      message: 'Error!',
      description: message
    });
  }, [error]);

  useEffect(displayErrorMessage, [error]);

  return (
    <>
      {loading && <PageLoading />}
      {redirectPath && <Redirect to={redirectPath} />}

      {!loading && (
        <Row
          style={{
            width: '100%',
            height: '100vh',
            padding: '3em 0'
          }}
          gutter={16}
        >
          <Col span={20} offset={2}>
            <Row type="flex" gutter={16}>
              <Col
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 24 }}
                lg={{ span: 8 }}
                xl={{ span: 8 }}
                xxl={{ span: 6 }}
              >
                {/* Profile */}
                <Profile />
              </Col>
              <Col
                xs={{ span: 24 }}
                sm={{ span: 24 }}
                md={{ span: 24 }}
                lg={{ span: 16 }}
                xl={{ span: 16 }}
                xxl={{ span: 18 }}
              >
                {MetricTypes.map(metricType => (
                  <Metric
                    key={`${metricType}`}
                    fetching={fetchingMetrics.includes(metricType) || false}
                    metric={metricType}
                    data={null}
                  />
                ))}
              </Col>
            </Row>
          </Col>
        </Row>
      )}
    </>
  );
};

const mapStateToProps = (state: any) => {
  const { auth } = state;
  const {
    loading,
    redirectPath,
    metricData,
    fetchingMetrics,
    error
  } = auth as IAuthState;
  return {
    metricData,
    loading,
    redirectPath,
    fetchingMetrics,
    error
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    login: (loginType: LoginTypes, parameters?: any) => {
      dispatch(actions.auth.login(loginType, parameters));
    },
    init: () => {
      dispatch(actions.auth.init());
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
