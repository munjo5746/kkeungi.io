import {
  Card,
  Typography,
  message,
  Tooltip,
  Icon,
  Result,
  Alert,
  Spin
} from 'antd';
import React from 'react';
import { connect } from 'react-redux';
import EllipsisButton from '../../shared/components/EllipsisButton';

import { MetricType, MetricTypeInfo } from '../../shared/types/metrics';

import WeekStats from './metric/WeekStats';
import HourStats from './metric/HourStats';
import LanguageDist from './metric/LanguageDist';
import { IAuthState } from '../../redux/reducers/auth';

interface IMetric {
  metric: MetricType;
  data: any;
  fetching: boolean;
}

const Metric: React.FC<IMetric> = ({ metric, data, fetching }) => {
  const metricTypeInfo = MetricTypeInfo.find(info => info.type === metric);

  if (!metricTypeInfo) {
    message.error(`Error while retrieving metric data.`);
  }

  if (!metricTypeInfo) {
    // TODO: show valid error message view
    return null;
  }

  return (
    <Spin tip="Fetching..." spinning={fetching}>
      <Card
        style={{
          position: 'relative'
        }}
      >
        <div>
          <Typography.Title level={4}>
            {metricTypeInfo.title}
            <Tooltip title={metricTypeInfo.description}>
              <Icon
                style={{
                  transform: 'scale(0.6)',
                  marginLeft: '0.2em'
                }}
                type="question"
              />
            </Tooltip>
          </Typography.Title>
        </div>
        <div
          style={{
            width: '100%',
            height: '20em'
          }}
        >
          {metric === 'week-commits' && (
            <WeekStats
              metric={metricTypeInfo.type}
              data={
                data
                  ? Object.keys(data).map(key => {
                      return {
                        x: key,
                        y: data[key]
                      };
                    })
                  : []
              }
            />
          )}

          {metric === 'hourly-commits' && (
            <HourStats metric={metricTypeInfo.type} data={data ? data : {}} />
          )}

          {metric === 'language-distribution' && (
            <LanguageDist
              metric={metricTypeInfo.type}
              data={
                data
                  ? Object.keys(data).map(key => {
                      return {
                        id: key,
                        label: key,
                        value: data[key]
                      };
                    })
                  : []
              }
            />
          )}
        </div>
      </Card>
    </Spin>
  );
};

const mapStateToProps = (state: any) => {
  const { auth } = state;
  const { user } = auth as IAuthState;

  return {
    user
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(Metric);
