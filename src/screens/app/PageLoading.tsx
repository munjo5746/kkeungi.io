import React from 'react';

import { Icon } from 'antd';

const PageLoading: React.FC = () => (
  <div
    style={{
      position: 'relative',
      background: 'white',
      width: '100%',
      height: '100vh'
    }}
  >
    <Icon
      style={{
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%,-50%)',
        fontSize: '3rem',
        color: '#FF4D4F'
      }}
      type="loading"
      spin={true}
    />
    <div
      style={{
        position: 'absolute',
        bottom: '1em',
        left: '50%',
        transform: 'translateX(-50%)',
        fontSize: '3rem',
        color: '#FF4D4F'
      }}
      className="korean"
    >
      끈기
    </div>
  </div>
);

export default PageLoading;
