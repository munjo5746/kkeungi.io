import React from 'react';
import { connect } from 'react-redux';

import UserInfo from './profile/UserInfo';

const Profile: React.FC = () => {
  return (
    <>
      <UserInfo />
    </>
  );
};

const mapStateToProps = (state: any) => {
  return {};
};
const mapDispatchToProps = (dispatch: any) => {
  return {};
};
export default connect(mapStateToProps, mapDispatchToProps)(Profile);
