import { ResponsiveLine } from '@nivo/line';
import React from 'react';
import { Alert } from 'antd';
import { MetricType } from '../../../shared/types/metrics';

interface IWeekStats {
  metric: MetricType;
  data: any;
}
const WeekStats: React.FC<IWeekStats> = ({ metric, data }) => {
  if (!data || data.length === 0) {
    return (
      <Alert
        showIcon={true}
        message="No data"
        description={`Failed to load ${metric}`}
        type="error"
      />
    );
  }

  return (
    <ResponsiveLine
      data={[
        {
          id: 'Commit',
          data
        }
      ]}
      curve="linear"
      margin={{ top: 50, right: 110, bottom: 50, left: 60 }}
      xScale={{ type: 'point' }}
      yScale={{ type: 'linear', stacked: true, min: 'auto', max: 'auto' }}
      axisTop={null}
      axisRight={null}
      axisBottom={{
        orient: 'bottom',
        tickSize: 5,
        tickPadding: 5,
        tickRotation: 0,
        legend: 'Week',
        legendOffset: 36,
        legendPosition: 'middle'
      }}
      axisLeft={{
        orient: 'left',
        tickSize: 5,
        tickPadding: 5,
        tickRotation: 0,
        legend: '# of commits',
        legendOffset: -40,
        legendPosition: 'middle'
      }}
      enableGridX={false}
      colors={{ scheme: 'category10' }}
      pointSize={10}
      pointColor={{ theme: 'background' }}
      pointBorderWidth={2}
      pointBorderColor={{ from: 'serieColor' }}
      pointLabel="y"
      pointLabelYOffset={-12}
      useMesh={true}
      legends={[
        {
          anchor: 'bottom-right',
          direction: 'column',
          justify: false,
          translateX: 110,
          translateY: 0,
          itemsSpacing: 0,
          itemDirection: 'left-to-right',
          itemWidth: 80,
          itemHeight: 20,
          itemOpacity: 0.75,
          symbolSize: 12,
          symbolShape: 'circle',
          symbolBorderColor: 'rgba(0, 0, 0, .5)',
          effects: [
            {
              on: 'hover',
              style: {
                itemBackground: 'rgba(0, 0, 0, .03)',
                itemOpacity: 1
              }
            }
          ]
        }
      ]}
    />
  );
};

export default WeekStats;
