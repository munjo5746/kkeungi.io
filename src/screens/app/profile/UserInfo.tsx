import React, { useEffect, useState } from 'react';

import {
  Alert,
  Badge,
  Button,
  Card,
  Col,
  Divider,
  Drawer,
  Dropdown,
  Form,
  Icon,
  Input,
  Menu,
  message,
  Modal,
  Row
} from 'antd';

import { connect } from 'react-redux';
import { IUser, LoginTypes, UserInfoField } from '../../../shared/types/auth';
import { IAuthState } from '../../../redux/reducers/auth';

import actions from '../../../redux/actions/index';

import TextArea from 'antd/lib/input/TextArea';
import EllipsisButton from '../../../shared/components/EllipsisButton';
import Label from '../../../shared/components/Label';

const socialLinks = [
  {
    iconName: 'github',
    url: ''
  },
  {
    iconName: 'linkedin',
    url: ''
  },
  {
    iconName: 'gitlab',
    url: ''
  }
];

const socialLoginButtons: Array<{
  title: string;
  type: LoginTypes;
  iconName: string;
  backgroundColor: string;
  url: string;
  disabled: boolean;
}> = [
  {
    title: 'Github',
    type: 'github',
    iconName: 'github',
    backgroundColor: '#24292D',
    url: `https://github.com/login/oauth/authorize?scope=user:email&client_id=${'d4481f7a4c41c91cea88'}`,

    disabled: false
  },
  {
    title: 'Gitlab',
    type: 'gitlab',
    iconName: 'gitlab',
    backgroundColor: '#E24329',
    url: '',
    disabled: true
  }
];

interface IUserInfo {
  user: IUser;
  savingUser: boolean;
  logoutInProgress: boolean;

  saveUser: (user: IUser) => void;
  logout: () => void;
}

const UserInfo: React.FC<IUserInfo> = ({
  user,
  savingUser,
  logoutInProgress,
  saveUser,
  logout
}) => {
  useEffect(() => {
    if (logoutInProgress) {
      message.loading('Logout in progress...');
    }
  }, [logoutInProgress]);

  const [userInfo, setUserInfo] = useState<IUser | undefined>(user);

  useEffect(() => {
    setUserInfo(user);
  }, [user]);

  const updateUserInfo = (event: any, key: UserInfoField) => {
    const { target } = event;
    const { value } = target;

    if (!userInfo) return;

    const updatedUser: IUser = {
      ...userInfo,
      [key]: value
    };

    setUserInfo(updatedUser);
  };

  const [displayLoginView, setDisplayLoginView] = useState(false);
  const [loginButtonLoading, setLoginButtonLoading] = useState(false);
  const [displayUserEditDrawer, setDisplayUserEditDrawer] = useState(false);
  const [, setDisplayRoleManageDrawer] = useState(false);

  const { email, firstName, lastName, roles, about } = userInfo || {
    email: 'alan@email.com',
    firstName: '',
    lastName: '',
    roles: [],
    about: ''
  };

  return (
    <>
      <Card
        style={{
          position: 'relative',
          marginBottom: '1.5em'
        }}
      >
        <img
          style={{
            width: '70%',
            height: '70%',
            borderRadius: '50%',
            border: '2px solid gray',
            display: 'block',
            margin: '0 auto'
          }}
          src="https://www.codechef.com/sites/default/files/uploads/pictures/2e44ba4de59ae4266ad984b53ee96a7f.jpg"
          alt="profile"
        />
        <div
          style={{
            width: '70%',
            margin: '1.5em auto'
          }}
        >
          <Row
            style={{
              margin: '2em 0'
            }}
            gutter={16}
            type="flex"
            justify="center"
          >
            {socialLinks.map(socialLink => (
              <Col
                key={`social-link-${socialLink.iconName}`}
                style={{
                  textAlign: 'center'
                }}
              >
                <Button
                  shape="circle"
                  icon={socialLink.iconName}
                  href={socialLink.url}
                />
              </Col>
            ))}
          </Row>
        </div>
        <Divider />
        <div
          style={{
            width: '100%'
          }}
        >
          <Label title="Email" text={email} />
        </div>
      </Card>

      <Modal
        title="Login"
        visible={displayLoginView}
        footer={null}
        onCancel={() => {
          setDisplayLoginView(false);
        }}
      >
        <Alert
          style={{
            marginBottom: '1em'
          }}
          type="info"
          showIcon={true}
          message="You will be login with one account"
          description="You can login with only one account. When you are about to add more data on the page, you will be able to select more accounts."
        />

        {socialLoginButtons.map(loginButton => (
          <Button
            key={`login-button-${loginButton.title}`}
            style={{
              background: loginButton.backgroundColor,
              border: 'none',
              marginBottom: '1em'
            }}
            href={loginButton.url}
            type="primary"
            icon={loginButton.iconName}
            size="large"
            block={true}
            disabled={loginButton.disabled}
            loading={!loginButton.disabled && loginButtonLoading}
            onClick={async () => {
              // the below function is only for visual user feedback.
              // when user clicks on the login button, ex. github, it redirects
              // user to the url set to the button attribute, "href" above^^^^^.
              // so here, onClick isn't doing anything for login.
              // NOTE: For github login, user will be re-directed to our site again with "code" returned from github.
              //       With the "code", the rest of the auth flow will be handled in App.tsx.
              setLoginButtonLoading(true);
            }}
          >
            {loginButton.title}
          </Button>
        ))}
      </Modal>
    </>
  );
};

const mapStateToProps = (state: any) => {
  const { auth } = state;
  const { user, savingUser, logoutInProgress } = auth as IAuthState;

  return {
    user,
    savingUser,
    logoutInProgress
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    saveUser: (user: IUser) => {
      // dispatch(actions.auth.saveUser(user));
    },
    logout: () => {
      dispatch(actions.auth.logout());
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserInfo);
