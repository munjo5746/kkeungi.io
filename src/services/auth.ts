import { ErrorCode, IUser } from '../shared/types/auth';
import config from '../config';

export const githubLogin = async (
  code: string
): Promise<{ result: any; error: { message: string } }> => {
  try {
    const response = await fetch('http://localhost:5000/auth/token', {
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json'
      },
      body: JSON.stringify({
        type: 'github',
        data: {
          code
        }
      })
    });

    const { result, error } = await response.json();

    return {
      result,
      error
    };
  } catch (error) {
    return {
      result: null,
      error: {
        message: 'Login failed.'
      }
    };
  }
};

export const init = async (
  userID: number
): Promise<{
  user?: IUser;
  error?: {
    message: string;
  };
}> => {
  try {
    const { result, error } = await fetch(
      `${config.services.auth.getUser({ id: userID })}`,
      {
        method: 'get',
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json'
        }
      }
    )
      .then(response => response.json())
      .catch(() => ({
        message: `There was an error!`
      }));

    if (error) {
      return {
        error
      };
    }

    const { user } = result;

    if (!user) {
      return {
        error: {
          message: 'There was server error.'
        }
      };
    }
    return {
      user
    };
  } catch (error) {
    return {
      error: {
        message: 'The app cannot be initialized. Check server status'
      }
    };
  }
};

export const saveUser = async (
  user: IUser
): Promise<{
  result: any;
  error: {
    code: ErrorCode;
    message: string;
    json: string;
  };
}> => {
  try {
    const response = await fetch(`http://localhost:5000/auth/user`, {
      method: 'put',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        user
      })
    });

    return await response.json();
  } catch (error) {
    return {
      result: null,
      error: {
        code: 'SAVE_USER_ERROR',
        message: 'There was an error while saving user information.',
        json: JSON.stringify(error)
      }
    };
  }
};
