import * as auth from './auth';
import * as metrics from './metrics';

export default {
  auth,
  metrics
};
