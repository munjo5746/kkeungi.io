import { MetricType } from '../shared/types/metrics';

interface IMetricData {
  metric: MetricType;
  data: any;
}

export const getMetrics = async (
  metrics: Array<{ metric: MetricType; url: string }>,
  complete: (metricData: IMetricData) => void
) => {
  const promises = metrics.map(obj => {
    return new Promise<{
      result: null | IMetricData;
      error: null | { message: string };
    }>(resolve => {
      return fetch(obj.url, {
        method: 'get'
      })
        .then(response => {
          return response.json();
        })
        .then(metricData => {
          resolve({
            result: metricData,
            error: null
          });
        })
        .catch(err =>
          resolve({
            result: null,
            error: {
              message: `${err}`
            }
          })
        );
    });
  });

  // for await (const { result, error } of promises) {

  // }
  // const data = await Promise.all(
  //   metrics.map(async obj => {
  //     try {
  //       const response = await fetch(obj.url, {
  //         method: 'get'
  //       });

  //       const { result, error } = await response.json();

  //       if (error) {
  //         return {
  //           metric: obj.metric,
  //           data: null
  //         };
  //       }

  //       return {
  //         metric: obj.metric,
  //         data: result[obj.metric]
  //       };
  //     } catch (error) {
  //       return {
  //         metric: obj.metric,
  //         data: null
  //       };
  //     }
  //   })
  // );

  // return data;
};
