import React from 'react';

import { Button, Icon } from 'antd';

interface IEllipsisButton {
  style?: object;
  onClick?: () => void;
}

const EllipsisButton: React.FC<IEllipsisButton> = ({ style = {}, onClick }) => {
  return (
    <Button
      style={{
        border: 'none',
        padding: 0,

        ...style
      }}
      onClick={onClick}
    >
      <Icon
        style={{
          fontSize: '1.3rem'
        }}
        type="ellipsis"
      />
    </Button>
  );
};

export default EllipsisButton;
