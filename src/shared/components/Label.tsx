import { Typography } from 'antd';
import React from 'react';

interface ILabel {
  title: string;
  text: string;
  editable?: boolean;
}
const Label: React.FC<ILabel> = ({ title, text, editable = false }) => {
  return (
    <div
      style={{
        margin: '2em 0',
        padding: '0 2em'
      }}
    >
      <Typography.Title type="secondary" level={4}>
        {title}
      </Typography.Title>
      <Typography.Text editable={editable} strong={true}>
        {text}
      </Typography.Text>
    </div>
  );
};

export default Label;
