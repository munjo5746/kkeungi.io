import React from 'react';

import { Icon, Spin } from 'antd';

const Loader: React.FC = (style: any) => {
  return (
    <div
      style={
        {
          width: '100%',
          minHeight: '21em',
          position: 'relative'
        } || style
      }
    >
      <Spin
        indicator={
          <Icon
            style={{
              position: 'absolute',
              left: '50%',
              top: '50%',
              transform: 'translate(-50%, -50%)',
              fontSize: '3rem'
            }}
            type="loading"
            spin={true}
          />
        }
      />
    </div>
  );
};

export default Loader;
