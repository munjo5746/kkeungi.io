export interface IUser {
  id: number;
  email: string;
  firstName: string;
  lastName: string;
  roles: any;
  about: string;
}

export enum AuthActionTypes {
  LOGIN_BEGIN = 'LOGIN_BEGIN',
  LOGIN_FAIL = 'LOGIN_FAIL',
  LOGIN_SUCCESS = 'LOGIN_SUCCESS',

  LOGOUT_BEGIN = 'LOGOUT_BEGIN',
  LOGOUT_COMPLETE = 'LOGOUT_COMPLETE',
  REDIRECT = 'REDIRECT',

  INIT_BEGIN = 'INIT_BEGIN',
  INIT_FAIL = 'INIT_FAIL',
  INIT_EXAMPLE = 'INIT_EXAMPLE',
  INIT_SUCCESS = 'INIT_SUCCESS',
  INIT_METRICS_COMPLETE = 'INIT_METRICS_COMPLETE',
  GET_METRICS_BEGIN = 'GET_METRICS_BEGIN',

  SAVE_USER_BEGIN = 'SAVE_USER_BEGIN',
  SAVE_USER_FAIL = 'SAVE_USER_FAIL',
  SAVE_USER_SUCCESS = 'SAVE_USER_SUCCESS'
}

export type LoginTypes = 'github' | 'gitlab';

export type ErrorCode = 'INIT_ERROR' | 'NOT_LOGGED_IN' | 'SAVE_USER_ERROR';

export type UserInfoField =
  | 'id'
  | 'email'
  | 'firstName'
  | 'lastName'
  | 'role'
  | 'location'
  | 'country'
  | 'about';
