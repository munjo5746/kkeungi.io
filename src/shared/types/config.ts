import { MetricType } from './metrics';
import { LoginTypes } from './auth';

export interface IConfig {
  services: IServices;
}

interface IServices {
  authorization: (host: string, cliendId: string) => string;
  auth: {
    getSocialLoginTokenUrl: ({ host }: ISocialLoginParamterType) => string;
    getUser: ({ id }: { id: number }) => string;
  };
  metrics: {
    getMetricUrl: ({
      host,
      metricType,
      userID,
      queryParameters
    }: IGetMetricParameterType) => string;
  };
}
export interface ISocialLoginParamterType {
  host: string;
}

export interface IGetMetricParameterType {
  host?: string;
  metricType: MetricType;
  userID: number;
  queryParameters?: string;
}
