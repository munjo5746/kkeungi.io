export const MetricTypes: MetricType[] = [
  'hourly-commits',
  'language-distribution',
  'week-commits'
];
export type MetricType =
  | 'week-commits'
  | 'hourly-commits'
  | 'language-distribution';

export const MetricTypeInfo: Array<{
  title: string;
  description: string;
  type: MetricType;
}> = [
  {
    title: 'Weekly Commits',
    description: '# of commits on a day within a week.',
    type: 'week-commits'
  },
  {
    title: 'Hourly Commits',
    description: '# of commits on each hour in a day.',
    type: 'hourly-commits'
  },
  {
    title: 'Language Distribution',
    description: 'language distributions among your projects',
    type: 'language-distribution'
  }
];
